package com.education.ua;

import java.util.HashMap;
import java.util.Map;

public class HashMapTask {
    public static void main(String[] args) {
        // ��������� HashMap, �� ���� - �� �����, � �������� - ��'��� ����� Product
        HashMap<String, Product> hashMap = new HashMap<>();

        // ��������� ��'���� ����� Product
        Product product1 = new Product("������", 18.50, "��������");
        Product product2 = new Product("���", 21.20, "��������");
        Product product3 = new Product("�������", 16.50, "��������");

        // ������ ��'���� �� HashMap, �������������� ��'� �� ����
        hashMap.put(product1.getName(), product1);
        hashMap.put(product2.getName(), product2);
        hashMap.put(product3.getName(), product3);

        // �������� �������� �� ������ "���" �� �������� ����
        System.out.println("hashMap.get(\"���\"): " + hashMap.get("���"));

        // ����������, �� ���� ���� "�������" � HashMap
        System.out.println("hashMap.containsKey(\"�������\"): " + hashMap.containsKey("�������"));

        // ����������, �� ���� ���� "���" � HashMap
        System.out.println("hashMap.containsKey(\"���\"): " + hashMap.containsKey("���"));

        // ����������, �� ���� �������� product1 � HashMap
        System.out.println("hashMap.containsValue(product1): " + hashMap.containsValue(product1));

        // �������� ������� ������ � HashMap
        System.out.println("hashMap.keySet(): " + hashMap.keySet());

        // ������� HashMap
        hashMap.clear();

        // ������ �������� product1 �� product2 � HashMap, ��� ���� ���� ����� �������
        hashMap.putIfAbsent(product1.getName(), product1);
        hashMap.putIfAbsent(product2.getName(), product1);

        // �������� �������� � ���������� � HashMap �� �������� ��
        System.out.println("hashMap.values(): " + hashMap.values());

        // ��������� ������ HashMap
        HashMap<String, Product> hashMap2 = new HashMap<>();

        // ������ product3 � ������ HashMap
        hashMap2.put(product3.getName(), product3);

        // ������ �� �������� � ������� HashMap � ������
        hashMap.putAll(hashMap2);

        // ��������� �������� product1 �� ������ product2 � ������� HashMap
        hashMap.remove(product2.getName(), product1);

        // �������� ������� �������� � HashMap �� �������� ��
        System.out.println("hashMap.size(): " + hashMap.size());

        // ���������� �� ���� ����-�������� � HashMap �� �������� ��
        for (Map.Entry<String, Product> entry : hashMap.entrySet()) {
            String key = entry.getKey();
            Product value = entry.getValue();
            System.out.println(key +" "+ value);
            // ������� �������� �� product3
            entry.setValue(product3);
        }

        // �������� �� �������� � HashMap
        System.out.println(hashMap.values());
    }
}
