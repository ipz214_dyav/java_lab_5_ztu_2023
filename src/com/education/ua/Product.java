package com.education.ua;

public class Product implements Comparable<Product> {
    private int id;
    private String name;
    private Double price;
    private String producer;
    private static int setID = 0;

    public Product(String name, Double price, String producer) {
        this.id = setID;
        this.name = name;
        this.price = price;
        this.producer = producer;
        setID++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    @Override
    public int compareTo(Product otherProduct) {
        return this.name.compareTo(otherProduct.name);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", producer='" + producer + '\'' +
                '}';
    }
}
