package com.education.ua;

import java.util.*;

public class ArrayDequeTask {
    public static void main(String[] args) {
        // ��������� ArrayDeque ��� ��'���� ����� Product
        ArrayDeque<Product> queue = new ArrayDeque<>();

        // ��������� ��'���� ����� Product
        Product product1 = new Product("������", 18.50, "���������");
        Product product2 = new Product("���", 21.20, "���������");
        Product product3 = new Product("�������", 16.50, "���������");

        // ������ ��'���� �� ����� �� ��������� ������ push
        queue.push(product1);
        queue.push(product2);

        // ������ ��'��� product3 � ����� ����� �� ��������� ������ offerLast
        queue.offerLast(product3);

        // �������� ��������� �����
        System.out.println("�������� ��������� �����:");
        queue.forEach(System.out::println);

        // �������� �� �������� ������ ������� ����� �� ��������� ������ getFirst
        System.out.println("queue.getFirst(): " + queue.getFirst());

        // �������� �� �������� �������� ������� ����� �� ��������� ������ peekLast
        System.out.println("queue.peekLast(): " + queue.peekLast());

        // ��������� �� �������� ������ ������� ����� �� ��������� ������ pop
        System.out.println("queue.pop(): " + queue.pop());

        // �������� ����� ���� ��������� ������� ��������
        System.out.println("�������� ����� ���� queue.pop():");
        queue.forEach(System.out::println);

        // ��������� �� �������� �������� ������� ����� �� ��������� ������ removeLast
        System.out.println("queue.removeLast(): " + queue.removeLast());

        // �������� �� �������� ������ ������� ����� �� ��������� ������ peek
        System.out.println("queue.peek(): " + queue.peek());

        // �������� ����� ���� ��������� ���������� �������� �� ��������� �������
        System.out.println("�������� ����� ���� queue.peek ():");
        queue.forEach(System.out::println);
    }
}
