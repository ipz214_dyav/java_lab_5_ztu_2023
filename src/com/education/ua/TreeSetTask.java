package com.education.ua;

import java.util.TreeSet;

public class TreeSetTask {
    public static void main(String[] args) {
        // ��������� TreeSet ��� ��'���� ����� Product
        TreeSet<Product> treeSet = new TreeSet<>();

        // ��������� ��'���� ����� Product
        Product product1 = new Product("������", 18.50, "���������");
        Product product2 = new Product("���", 21.20, "���������");
        Product product3 = new Product("�������", 16.50, "���������");

        // ������ ��'���� �� TreeSet
        treeSet.add(product1);
        treeSet.add(product2);
        treeSet.add(product3);

        // �������� ������ ������� � TreeSet
        System.out.println("treeSet.first(): " + treeSet.first());
        // �������� �������� ������� � TreeSet
        System.out.println("treeSet.last(): " + treeSet.last());
        // �������� ��������� ��������, �� ����� �� product3
        System.out.println("treeSet.headSet(product3): " + treeSet.headSet(product3));
        // �������� ��������� ��������, �� ����������� �� product1 � product3
        System.out.println("treeSet.subSet(product1, product3): " + treeSet.subSet(product1, product3));
        // �������� ��������� ��������, �� ����� ��� ���� �� product2
        System.out.println("treeSet.tailSet(product2): " + treeSet.tailSet(product2));
        // �������� ��������� �������, ���� ������ ��� ����� product2
        System.out.println("treeSet.ceiling(product2): " + treeSet.ceiling(product2));
        // �������� ��������� �������, ���� ������ ��� ����� product2
        System.out.println("treeSet.floor(product2): " + treeSet.floor(product2));
        // �������� ��������� ������� ���� product2 (���� ����)
        System.out.println("treeSet.higher(product2): " + treeSet.higher(product2));
        // ��������� �� �������� ������ �������
        System.out.println("treeSet.pollFirst(): " + treeSet.pollFirst());
        // ��������� �� �������� �������� �������
        System.out.println("treeSet.pollLast(): " + treeSet.pollLast());
        // �������� TreeSet � ����������� �������
        System.out.println("treeSet.descendingSet(): " + treeSet.descendingSet());
    }
}
